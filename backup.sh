#!/bin/bash

# Check dependencies
curl --version > /dev/null || (echo 'Please install curl'; exit 1)
git  --version > /dev/null || (echo 'Please install git';  exit 1)
jq   --version > /dev/null || (echo 'Please install jq';   exit 1)
sed  --version > /dev/null || (echo 'Please install sed';  exit 1)
wget --version > /dev/null || (echo 'Please install wget'; exit 1)

# Find the directory of this script
dir="$(dirname "$(readlink -f "$0")")"

# Establish the data directory and its git repository
if [[ ! -e "$dir/data" ]]; then
  mkdir "$dir/data"
fi
if [[ ! -e "$dir/data/.git" ]]; then
  git init "$dir/data"
fi

# Read the token, generate the authorization header
if [[ ! -e "$dir/.token" ]]; then
  echo 'No .token file found. Please review the README.'
  exit 1
else
  token="$(cat "$dir"/.token)"
  auth="Authorization: Bearer $token"
fi

# Download the JSON data
base='https://api.ouraring.com/v1'
q='start=2013-01-01'
function format () { # JSON with indentation but compact arrays of numbers
  jq . | sed -E -e ':a' -e 'N' -e '$!ba' -e 's/\n\s+([0-9\.\-]+,?)/\1/g' -e 's/([0-9]+)\n\s+\]/\1]/g'
}
curl -s -H "$auth" "$base/userinfo"     | format > "$dir/data/userinfo.json"
curl -s -H "$auth" "$base/sleep?$q"     | format > "$dir/data/sleep.json"
curl -s -H "$auth" "$base/activity?$q"  | format > "$dir/data/activity.json"
curl -s -H "$auth" "$base/readiness?$q" | format > "$dir/data/readiness.json"
curl -s -H "$auth" "$base/bedtime?$q"   | format > "$dir/data/bedtime.json"

# Download the API documentation
rm -f "$dir/data/docs/"*.html
wget 'https://cloud.ouraring.com/docs/' \
  --recursive --no-parent --html-extension --convert-links --no-directories \
  --directory-prefix="$dir/data/docs" --quiet
sed -i '/csrf_token/d' "$dir/data/docs/"*.html

# Commit data to its git repository
git -C "$dir/data" add .
git -C "$dir/data" commit --allow-empty --message='Automatic commit.'

# Done
echo 'Oura backup complete'
