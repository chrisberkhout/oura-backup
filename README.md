# oura-backup

Back up your data from the [Oura Ring](https://ouraring.com/) sleep and
activity tracker.

This script will download a full copy of all the data avilable from the
[Oura API](https://cloud.ouraring.com/docs/). It will be nicely formatted and
tracked in git, so that each new backup can replace the previous one (in git's
working copy) without risking the loss of earlier data or retaining redundant
copies.

## Dependencies

This script is written for bash and depends on some common command line tools
that you may need to install:
[curl](https://curl.se/),
[git](https://git-scm.com/),
[jq](https://stedolan.github.io/jq/),
[sed](https://en.wikipedia.org/wiki/Sed),
[wget](https://www.gnu.org/software/wget/).

## Setup

Clone this repository:

```bash
git clone git@gitlab.com:chrisberkhout/oura-backup.git
cd oura-backup
```

Log in to [Oura Cloud](https://cloud.ouraring.com/account/login) and create a
[Personal Access Token](https://cloud.ouraring.com/docs/authentication#create-a-personal-access-token).
Copy that token and write it into a `.token` file for the backup script to
find:

```bash
echo 'YOUROURAPERSONALACCESSTOKEN' > .token
chmod go-rwx .token
```

## Usage

Just run the backup script:

```bash
./backup.sh
```

Your data will appear in the `./data/` directory, the contents of which are
tracked in a separate git repository.

## Additional notes

The `./data/docs/` directory will have a snapshot of the Oura API documentation
to aid in the interpretation of the data.

You can rerun the backup script as often as you like. If the script breaks you
can restore earlier backup data from git.

If you run the script from elsewhere the `./data/` directory will still be in
the same directory as the script.

In the [Trends](https://cloud.ouraring.com/trends) section of Oura Cloud, you
download data for a particular time range as a single CSV file. That may be a
slightly more convenient format for some uses, but it's less complete than the
JSON data downloaded by this script.
